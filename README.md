# Project "Iterative"
[![Snakemake](https://img.shields.io/badge/snakemake-≥3.9.1-brightgreen.svg?style=flat-square)](https://snakemake.bitbucket.io)

Title of last presentation:
"Iterative and semi-automatic strain identification of microbial tandem MS spectra"
Mathias Kuhring, Bernhard Y. Renard, 2016, Metaproteomics Symposium Magdeburg

## Installation

### Download

Download the pipeline from the repository using git and change to the directory:

    git clone --recursive --depth=1 https://gitlab.com/mkuhring/iterative.git
    cd iterative

### Dependencies

Dependencies are easily installed via conda. If conda is not yet available on your system install Miniconda via:

    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh; chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh

Then install the dependencies using conda:

    conda env create --file environment.yaml

You could also modify the [environment.yaml](environment.yaml) to exclude common software which is already available in your PATH environment. Similar or newer versions might work as well, though Snakemake should be at least version 3.9.1.

To compile and execute the pipeline, activate the new conda environment:

    source activate iterative

After using the pipeline, you may deactivate the conda environment via:

    source deactivate iterative

A few python modules rely on Python 2 interpreter and libraries. Those are not installed via conda yet since conda doesn’t support different versions of python in the same environment. However, they can be automatically installed in a separated environment on pipeline execution using the "--use-conda" parameter. Otherwise, the pipeline will expect a python2 executable available via your PATH environment (see [environments/python2.yaml ](environments/python2.yaml) for additional library dependencies).

### Compiling

The pipeline makes use of several java modules which need to be compiled before execution. Use the Maven build system (available in the active conda environment) to automatically resolve dependencies and create required jar files:

    cd modules_java
    mvn clean package


## Execution [TODO]

1) Go to workdir, folder were the sample should be analyzed
2) Create configuration [ADD EXAMPLE]

Example call [TEMPORARY]:

    snakemake -s ~/workspace/iterative/Snakefile --configfile snakemake.config.json --cores 4 --use-conda -pkn
